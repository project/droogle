<?php

namespace Drupal\droogle\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure site information settings for this site.
 */
class DroogleSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a SiteInformationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);

    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'droogle_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['droogle.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $droogle_config = $this->config('droogle.settings');

    $form['sitewide'] = [
      '#type' => 'details',
      '#title' => $this->t('Sitewide Droogle settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => $this->t(
        'To create client IDs, secret: <ol>
          <li>Visit <a href="@url">Google Console</a> and create a project to use.</li>
          <li>Enable the Drive API and the Drive SDK under APIs tab</li>
          <li>Generate client IDs/Secret under the Credentials tab</li>
          <li>Add @url_refresh to "Redirect URIs" for the new client ID. Redirect URI should be @redirect_url</li>
          </ol>
          For detailed documentation check <a href="https://www.drupal.org/docs/contributed-modules/google-api-php-client/google-api-console-configurations">Google API Console Configurations</a>',
        [
          '@url' => 'https://cloud.google.com/console',
          '@redirect_url' => google_api_client_callback_url(),
        ]
      ),
    ];

    $form['sitewide']['droogle_title_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Droogle Page Title'),
      '#default_value' => $droogle_config->get('title_text'),
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 64,
      '#description' => $this->t(
        'Enter the title to put at the top of the <a href="@url">droogle page</a> (when not within an Organic Groups context),
        default is: "DROOGLE: A list of your google docs"',
        [
          '@url' => 'droogle',
        ]
      ),
    ];
    $form['google_api_client_id'] = [
      '#type' => 'hidden',
      '#value' => $droogle_config->get('google_api_client_id'),
    ];

    if ($google_api_client_id = $droogle_config->get('google_api_client_id')) {
      $google_api_client = $this->entityTypeManager->getStorage('google_api_client')->load($google_api_client_id);
      if (!$google_api_client->getAuthenticated()) {
        if ($droogle_config->get('separate_drives_for_users')) {
          $this->messenger()->addMessage($this->t('You have configured separate drives for users so system will ask you to connect drive when you try to access connector.  You need to add permission "Authenticate Droogle Google Api Client account" to roles who can use the api'));
        }
        else {
          $authenticate_link = Link::createFromRoute($this->t('Authenticate now'),
            'google_api_client.callback',
            [
              'id' => $google_api_client->getId(),
              'destination' => '/admin/config/content/droogle',
            ],
            ['attributes' => ['target' => '_blank']])->toString();
          $this->messenger()->addWarning($this->t('You need to authenticate with google, %link', ['%link' => $authenticate_link]));
        }
      }
      else {
        if ($droogle_config->get('separate_drives_for_users')) {
          $this->messenger()->addMessage($this->t('You have configured separate drives for users so system will ask you to connect drive when you try to access connector.  You need to add permission "Authenticate Droogle Google Api Client account" to roles who can use the api'));
        }
        $this->messenger()->addMessage($this->t('Droogle is now ready for use.'));
      }
    }
    $form['sitewide']['droogle_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => is_object($google_api_client) ? $google_api_client->getClientId() : '',
      '#required' => TRUE,
      '#size' => 100,
      '#maxlength' => 150,
      '#description' => $this->t('The site wide google client id.'),
    ];

    $form['sitewide']['droogle_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#default_value' => is_object($google_api_client) ? $google_api_client->getClientSecret() : '',
      '#required' => TRUE,
      '#description' => $this->t('The site wide google client secret.'),
    ];

    $form['sitewide']['droogle_drive_connect_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Access Type'),
      '#default_value' => is_object($google_api_client) ? $google_api_client->getAccessType() : TRUE,
      '#options' => [$this->t('Online'), $this->t('Offline')],
      '#required' => TRUE,
      '#description' => $this->t('Access type defines when can this authentication be used,
       if online then it can be used only when the user is logged in on google and authenticates,
       once authentication expires usually in 1 hour it can not be used,
       if offline then can be used even when user is logged out.'),
    ];
    $separate_drives = $droogle_config->get('separate_drives_for_users');
    $form['sitewide']['droogle_separate_drives_for_users'] = [
      '#type' => 'select',
      '#title' => $this->t('Separate Drives for Users'),
      '#default_value' => $separate_drives ? $separate_drives : FALSE,
      '#options' => [$this->t('No'), $this->t('Yes')],
      '#required' => TRUE,
      '#attributes' => ['disabled' => !$this->moduleHandler->moduleExists('gauth_user')],
      '#description' => $this->t('This defines whether the same google drive is shared across all users of the module or each user authenticates his/her owne drive.
      If we are using single drive then it is recommended to have access type offline else each user needs to reauthenticate when trying to access.
      If this configuration is disabled and you want to have separate drives authenticated then you need to download <a href="https://drupal.org/project/gauth_user">Google Authentication for Users</a>'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['google_api_client_id']) {
      $google_api_client = $this->entityTypeManager->getStorage('google_api_client')->load($values['google_api_client_id']);
      $google_api_client->setClientId($values['droogle_client_id']);
      $google_api_client->setClientSecret($values['droogle_client_secret']);
      $google_api_client->setAccessType($values['droogle_drive_connect_mode']);
    }
    else {
      $account = [
        'name' => 'Droogle Google Api Client',
        'client_id' => $values['droogle_client_id'],
        'client_secret' => $values['droogle_client_secret'],
        'access_token' => '',
        'services' => ['drive'],
        'is_authenticated' => FALSE,
        'scopes' => ['DRIVE'],
        'access_type' => $values['droogle_drive_connect_mode'],
      ];
      $google_api_client = $this->entityTypeManager->getStorage('google_api_client')->create($account);
    }
    $google_api_client->save();
    if ($values['droogle_separate_drives_for_users']) {
      $config = $this->configFactory->getEditable('gauth_user.settings');
      $google_api_clients = $config->get('google_api_clients');
      $google_api_clients[] = $google_api_client->id();
      $config->set('google_api_clients', $google_api_clients)->save();
    }
    $values['google_api_client_id'] = $google_api_client->getId();
    $this->config('droogle.settings')
      ->set('title_text', $values['droogle_title_text'])
      ->set('google_api_client_id', $values['google_api_client_id'])
      ->set('separate_drives_for_users', $values['droogle_separate_drives_for_users'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
