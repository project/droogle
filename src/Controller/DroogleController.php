<?php

namespace Drupal\droogle\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\droogle\Service\DroogleConnectorService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for System routes.
 */
class DroogleController extends ControllerBase {

  /**
   * The droogle connector.
   *
   * @var \Drupal\droogle\Service\DroogleConnectorService
   */
  protected $droogleConnector;

  /**
   * DroogleController constructor.
   *
   * @param \Drupal\droogle\Service\DroogleConnectorService $droogleConnector
   *   The Droogle Connector service.
   */
  public function __construct(DroogleConnectorService $droogleConnector) {
    $this->droogleConnector = $droogleConnector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('droogle.connector'),
    );
  }

  /**
   * Callback for main droogle page.
   */
  public function droogleNavigator() {
    $result = $this->droogleConnector->droogleOpenGdrive();

    return $result;
  }

}
