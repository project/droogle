<?php

namespace Drupal\droogle\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\google_api_client\Service\GoogleApiClientService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Droogle connector.
 *
 * @package Drupal\droogle
 */
class DroogleConnectorService {

  use StringTranslationTrait;

  /**
   * Request variable.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Google API Client Service.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiClientService
   */
  protected $googleApiClientService;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The system theme config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * DroogleConnector constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\google_api_client\Service\GoogleApiClientService $googleApiClient
   *   The google api client service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The logged in user account.
   */
  public function __construct(MessengerInterface $messenger, TranslationInterface $string_translation, ConfigFactoryInterface $config_factory, GoogleApiClientService $googleApiClient, RequestStack $request, EntityTypeManagerInterface $entityTypeManager, AccountInterface $account) {
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->configFactory = $config_factory;
    $this->googleApiClientService = $googleApiClient;
    $this->request = $request;
    $this->entityTypeManager = $entityTypeManager;
    $this->account = $account;
  }

  /**
   * Connect to Google Drive in online mode.
   *
   * To let user work with GDrive in Drupal through the web interface.
   */
  public function droogleGdriveConnect($redirect_url = NULL) {
    if ($redirect_url == NULL) {
      $redirect_url = '/admin/content/droogle';
    }
    $config = $this->configFactory->get('droogle.settings');
    $google_api_client_id = $config->get('google_api_client_id');
    $google_api_client = '';
    if ($config->get('separate_drives_for_users')) {
      $properties = [
        'google_api_account' => $google_api_client_id,
        'uid' => $this->account->id(),
      ];
      $google_api_client = $this->entityTypeManager->getStorage('gauth_user')->loadByProperties($properties);
      if (empty($google_api_client)) {
        $google_api_client = $this->entityTypeManager->getStorage('gauth_user')->create($properties);
        if (!$google_api_client->save()) {
          $this->messenger->addError($this->t('Error occurred in creating authentication call'));
          return new RedirectResponse('<front>');
        }
      }
      else {
        $google_api_client = array_values($google_api_client);
        $google_api_client = $google_api_client[0];
      }
      $this->googleApiClientService->setGoogleApiClient($google_api_client);
    }
    else {
      $google_api_client = $this->entityTypeManager->getStorage('google_api_client')->load($google_api_client_id);
    }
    if (!$google_api_client) {
      $this->messenger->addError($this->t('Error occurred in creating authentication call'));
      return new RedirectResponse('<front>');
    }
    $this->googleApiClientService->setGoogleApiClient($google_api_client);
    $service = new \Google_Service_Drive($this->googleApiClientService->googleClient);

    // Disconnect from google drive.
    if ($this->request->getCurrentRequest()->get('logout')) {
      $google_api_client->setAccessToken('');
      $google_api_client->setAuthenticated(FALSE);
      $google_api_client->save();
      $destination = $this->request->getCurrentRequest()->get('destination');
      $logout_redirect_url = $destination != NULL ? $destination : $this->getRedirectUri();
      return new RedirectResponse(Url::fromUserInput($logout_redirect_url)->toString());
    }

    $return_result = [];
    if ($google_api_client->getAuthenticated()) {
      $return_result['disconnect_link'] = $this->getDisconnectLink();
      $return_result['#service'] = $service;
    }
    // We should be authorized.
    else {
      $return_result['auth_link'] = Link::createFromRoute($this->t('Connect Me to Google Drive!'),
        'google_api_client.callback',
        [
          'id' => $google_api_client->getId(),
          'type' => $google_api_client->getEntityTypeId(),
          'destination' => $redirect_url,
        ],
        )->toRenderable();
      $return_result['auth_link']['#attributes']['class'] = ['connect-gdrive'];
    }

    return $return_result;
  }

  /**
   * Connect to Google Drive in offline mode.
   *
   * To let Drupal work with Google Drive in background mode.
   */
  public function droogleGdriveConnectOffline() {
    return new \Google_Service_Drive($this->googleApiClientService->googleClient);
  }

  /**
   * Returns data from google drive to render it on your webpage.
   *
   * @param string $redirect_url
   *   Url which will be used as destination for redirect after the successful
   *   auth in Google Drive.
   *
   * @return array|RedirectResponse
   *   Returns array or a redirect.
   */
  public function droogleOpenGdrive($redirect_url = NULL) {
    // Connect to the google drive.
    $result = $this->droogleGdriveConnect($redirect_url);
    if (!is_array($result)) {
      return $result;
    }

    $client = $this->googleApiClientService->googleClient;
    $service = $result['#service'];

    $refresh_token = $this->googleApiClientService->googleClient->getRefreshToken();

    // If we have refresh token and access token is expired then try to refresh
    // the token.
    if (!empty($refresh_token) && $client->isAccessTokenExpired()) {
      try {
        $client->refreshToken($refresh_token);
      }
      catch (\Google_Auth_Exception $e) {
        // Something went wrong. We have to auth again.
        $content = [
          'auth_link' => $result['auth_link'],
        ];

        return $content;
      }
    }
    // Show results.
    if ($client->getAccessToken()) {
      $content = [
        '#theme' => 'droogle_list_files',
        '#service' => $service,
        '#disconnect_link' => $result['disconnect_link'],
        '#attached' => [
          'library' => [
            'droogle/droogle.main',
          ],
        ],
      ];
    }
    // Show url to auth.
    elseif (!empty($result['auth_link'])) {
      $content = [
        'auth_link' => $result['auth_link'],
      ];
    }
    return $content;
  }

  /**
   * Returns link to disconnect the GDrive.
   */
  private function getDisconnectLink() {
    $query = [
      'logout' => TRUE,
      'destination' => $this->request->getCurrentRequest()->getPathInfo(),
    ];

    $url = Url::fromRoute('droogle', [], ['query' => $query]);
    $link = Link::fromTextAndUrl('Disconnect me from Google Drive!', $url)->toRenderable();
    $link['#attributes']['class'] = ['disconnect-gdrive'];
    return $link;
  }

  /**
   * Returns redirect uri for google drive client.
   */
  private function getRedirectUri() {
    return google_api_client_callback_url();
  }

}
